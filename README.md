# PhD Thesis Olga Soloveva 2021



This is PhD thesis devoted to transport properties of the strongly-interacting matter created in heavy-ion collisions written by Olga Soloveva (2021) under supervision of Prof. Dr. Elena Bratkovskaya (the PHSD group) Frankfurt Goethe University.
 - For the transport simulations - a microscopic off-shell transport approach - [PHSD code](http://theory.gsi.de/~ebratkov/phsd-project/PHSD/index1.html) was employed 

Code development/simulations were conducted during 2018-2021
![image info](./plots/3PNJL/2d_press_pi_phase_PNJL.png)